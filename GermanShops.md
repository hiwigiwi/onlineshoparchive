### Preisvergleiche
- [geizhals.de](https://geizhals.de/): Speziell für Elektronik
- [idealo.de](https://www.idealo.de/): Elektronik und mehr
- [hardwareschotte.de](https://www.hardwareschotte.de/): Speziell für PC-Komponenten und Zubehör.

### Online Warenhäuser
- [manufactum.de](https://www.manufactum.de/): Fokus auf 'traditionell' hergestellte Produkte
- [otto.de](https://www.otto.de/):
- [galeria.de](https://www.galeria.de/): Galeria-Karstadt-Kaufhof
- [memolife.de](https://www.memolife.de/): Bio, öko & fair Trade Produkte, speziell Schreibwaren, aber auch Bekleidung, Möbel, etc.

--------------------------------------------------------------------------------------------------------
### BÜCHER
----
- [foliosociety.com](https://www.foliosociety.com/): Gebundene Ausgaben mit künstelerischem Cover
- [genialokal.de](https://www.genialokal.de): Ermöglicht bei einem lokalen Buchladen zu bestellen.
- [buch7.de](https://buch7.de/): Sozialer Buchladen, der einen Teil der Einkünfte spendet.
- [worldofbooks.com](https://www.worldofbooks.com/de-de): 
- [medimops.de](https://www.medimops.de/):
- [booklooker.de](https://www.booklooker.de/):
- [betterworldbooks.com](https://www.betterworldbooks.com/): Für englishsprachige Bücher
- [abebooks.de](https://www.abebooks.de/): Für gebracuhte Bücher, aber speziell für frühere Editionen und Erstausgaben
- [steidl.de](https://steidl.de/): Speziell für Fotografiebände
- [zvab.com](https://www.zvab.com/): Spezialisiert auf deutsche, antiquare Bücher.

#### Comics
- [comicshop.de](https://www.comicshop.de/): Aktuelle Comics, wenig ältere.
- [comicland.de](https://www.comicland.de/): Viel ältere Comics
- [uscomicversand.de](https://www.uscomicversand.de/): Speziell Amerikanische Superhelden Comics
- [sammlerecke.de](https://www.sammlerecke.de/): Sammlerstücke und Antiquitäten
- [comicexpress.de](https://www.comicexpress.de/): Neue und alte Comics

--------------------------------------------------------------------------------------------------------
### MUSIK
----
#### Instrumente

#### CDs & Schallplatten
- [discogs.com](https://www.discogs.com/): Marketplace für CDs, Vynil & Kasetten
--------------------------------------------------------------------------------------------------------
### DIY
----
#### Handwerkzeuge
- [contorion.de](https://www.contorion.de/): Schrauben, Handwerkzeug, Powertools, etc.
- [dictum.com](https://www.dictum.com/de/): Hand- & Elektrowerkzeuge: Messer, Sägen, Hobel, etc., aber auch Materialen: Hölzer, Stein, Leder, etc.
- [feinwerkzeuge.de](https://www.feinewerkzeuge.de/): Fast ausschliesslich Handwerkzeuge zum Basteln, Bauen, Werken.
- [hoffmann-group.de](https://www.hoffmann-group.com/DE/de/hom/): Zerspan-Werkzeuge.

#### Schreiben & Kaligraphie
- [feder-fuehrend.de](https://www.feder-fuehrend.de/): Kaligraphie Zubehör: Federn, Tinten, Papier.
- [fountainfeder.de](http://www.fountainfeder.de/): Fountain pens, Tinten, Papier
- [papierundstift.de](https://papierundstift.de/): Stifte, Tinten, Papier
- [purepens.co.uk](https://www.purepens.co.uk/): UK-based Shop für Kaligraphie Zubehör

#### Messer
- [nordisches-handwerk.de](https://www.nordisches-handwerk.de/): Alles zu Klingen: Messer, Äxte, Schleifen etc., Messermacherbedarf: Rohstahl, Griffe, Öfen, etc.
- [zubels-shop.com](https://www.zubels-shop.com/): Hölzer und Werkzeuge für Messermacher

--------------------------------------------------------------------------------------------------------
### ELEKTRONIK
----
#### Generell
- [1001lights.com](https://www.1001lights.com/de-de/): Lampen und Leuchten
- [3djake.de](https://www.3djake.de/): 3D Drucker und Zubehör
- [conrad.de](https://www.conrad.de/): Elektronikkomponenten und Verbraucher-Elektronik
- [develektro.com](https://www.develektro.com/): Spziell für Elektronikkomponenten für Prototyping
- [digikey.de](https://www.digikey.de/): DIY-Kits und Komponenten, viele gute Projektanleitungen
- [lampenwelt.de](https://www.lampenwelt.de/): Lampen und Leuchten
- [mindfactory.de](https://www.mindfactory.de/): Gamer-orientierte PC Hardware, aber auch Laptops und PC Zubehör.
- [voelkner.de](https://www.voelkner.de/): Verbraucher-Elektronik
- [rebuy.de](https://www.rebuy.de/): Gebrauchte Elektroprodukte, Teils refurbished.
- [reichelt.de](https://www.reichelt.de/): Alle Elektronikkomponenten und Teils Verbraucher-Elektronik
- [timbertraces.de](https://timbertraces.de/): Alles aus Holz: Wireless Charger, Handyhüllen, Lampen 

#### Computer
- [alternate.de](https://m.alternate.de/): Fokus auf PC-Komponenten, aber auch fertige Systeme, Zubehör und ein Outlet-Store.
- [arlt.com](https://www.arlt.com/): Fertige Builds, Komponenten, Server, aber auch Software.
- [caseking.de](https://www.caseking.de/): PC Komponenten und Zubehör für Gamer
- [cyberport.de](https://www.cyberport.de/): Desktops, Laptops, Tablets, Smartphones, Drucker, etc.
- [hardwarerat.de](https://hardwarerat.de/): Kleiner Shop für Komponenten und Builds, Laptops
##### Tastaturen
- [candykeys.com](https://candykeys.com/): Mechanical Keyboards, Switches und Keycaps
- [keyboard.io](https://shop.keyboard.io/): Kleiner Shop mit Switches & Keycaps
- [keycapsss.com](https://keycapsss.com/): Keyboard Komponenten zum Selberbauen
- [mechboards.co.uk](https://mechboards.co.uk/): UK-based Shop für Komponenten zum Selberbauen. Auch mit gutem Blog dazu.
- [mechsupply.co.uk](https://www.mechsupply.co.uk/): UK-based Shop für Mechanical Keyboards: fertige Keyboards & Komponenten zum Selberbauen
- [oblotzky.industries](https://oblotzky.industries/): Hersteller von Mechanical Keyboards
##### Single-Board Computers
- [berrybase.de](https://www.berrybase.de/): Raspberry Pis, Arduinos und Zubehör.

#### Batterien
- [akkuteile.de](https://www.akkuteile.de/): Speziell für alle Arten Batterien und Akkus, sammt Ladegeräte.
- [nkon.nl](https://www.nkon.nl/de/): Niederländischer Batterie-Fachhandel, Einwegbatterien & Akkus

#### Veranstaltungstechnik
- [banzaimusic.com](https://www.banzaimusic.com/home.php): Musikintrumenten Zubehör und Komponenten, speziell für E-Gitarren und Verstärker.
- [schneidersladen.de](https://schneidersladen.de/): Synthesizer Module
- [steinigke.de](https://www.steinigke.de/): Veranstaltungstechnik
- [thomann.de](https://www.thomann.de/intl/index.html): Instrumente, Zubehör, Veranstaltungstechnik
- [thonk.co.uk](https://www.thonk.co.uk/): DIY-Kits für Synthesizer
- [tubeampdoctor.com](https://www.tubeampdoctor.com/): Elektronik Komponenten für Lautsprecher und Verstärker.
- [tube-town.net](https://www.tube-town.net/ttstore/): Fokus auf Elektronikkomponenten für Musikinstrumente, Lautsprecher und Verstärker, aber auch einiges an Veranstaltungstechnik.

#### Kameras & Video
- [foto-video-sauter.de](https://www.foto-video-sauter.de/): Neues & gebracuhtes Equipment
- [videodata.de](https://www.videodata.de/): Kameras und Zubehör

---------------------------------------------------------------------------------------------------------
### OUTDOORS
----
- [bergfreunde.de](https://www.bergfreunde.de/): Bekleidung, Schuhe & Ausrüstung mit Fokus auf Nachhaltigkeit.
- [funktionelles.de](https://www.funktionelles.de/): Alle bekannten Marken in Bekleidung und Ausrüstung.
- [sackundpack.de](https://www.sackundpack.de/): Bekleidung, Ausrüstung, Zubehör, etc.
- [trekking-lite-store.com](https://www.trekking-lite-store.com/): Spezialisiert auf leichte Bekleidung und Ausrüstung.
- [unterwegs.biz](https://www.unterwegs.biz/): Bekleidung, Schuhe & Ausrüstung mit Fokus auf Nachhaltigkeit.
- [raeer.com](https://www.raeer.com/): Army-Surplus, gebraucht und neu, aber auch Campingausrüstung, etc.

#### Fahrrad
- [bike-discount.de](https://www.bike-discount.de/): Fahrräder, Komponenten und generell Outdoor.
- [bike-components.de](https://www.bike-components.de/): Fast ausschliesslich Fahrrad Komponenten
- [bike24.de](https://www.bike24.de/): Radfahren, Laufen, Schwimmen Equipment
- [enjoyyourbike.com](https://www.enjoyyourbike.com/): Speziell für Rennräder aber auch Radsport generell und Triathlon 
- [kurbelix.de](https://www.kurbelix.de/): Ausschliesslich  Fahrrad Komponenten
- [rosebikes.de](https://www.rosebikes.de/): Räder, Zubehör & Bekleidung

---------------------------------------------------------------------------------------------------------
### BEKLEIDUNG
----
- [avocadostore.de](https://www.avocadostore.de/): Herren- & Damenbekleidung mit Fokus auf Eco-Fashion.
- [grundstoff.de](https://www.grundstoff.net/): Fair-Trade Klmaotten, einfach gehalten, meist einfarbig.
- [happy-camel.de](https://www.happy-camel.de/): Spezialshop für Socken, Mützen & Handschuhe aus Kamel-, Yak- und Schafswolle.
- [outofaustralia.de](https://outofaustralia.de/): Spezialisiert auf Australische Klamotten
- [ubup.com](https://www.ubup.com/): "Used but precious"
- [raubdruckerin.de](https://raubdruckerin.de/en/shop/): T-Shirts, Hoodies, Jute-Beutel bedruckt mit  Hilfe von Gullideckeln aus verschiedenen Städten

#### Accessoires
- [guertelschnallen.com](https://www.guertelschnallen.com/): Ledergürtel, Schnallen und Buckle
- [poljot24.de](https://www.poljot24.de/): Spezialist für russusche Uhren seit 1992

--------------------------------------------------------------------------------------------------------
### LEBENSMITTEL
----
- [chili-shop24.de](https://www.chili-shop24.de/): Chilischoten, Chilisaucen, Chiliwurst, etc.
- [gepa-shop.de](https://www.gepa-shop.de/): Fairtrade Tee, Kaffee, Schokolade, aber auch Kleidung, Deko, etc.
- [kraeuter-und-tee.de](https://www.kraeuter-und-tee.de/index.php): Kräuter und Gewürze in allen Formen
- [pikantum.de](https://www.pikantum.de/): Getrocknete Kräuter und Gewürze, aber auch Tees und Feinkost
- [safran-tuebingen.de](https://www.safran-tuebingen.de/): Schokolade, Gewürze und Feinkost
- [zimtstangl.de](https://www.zimtstangl.de/shop.html): Getrocknete, aber auch frische, Kräuter und Gewürze.

#### Tee & Kaffee
- [cafe-libertad.de](https://www.cafe-libertad.de/): Bio und Fair-Trade Espresso und Kaffee
- [coffeecircle.com](https://www.coffeecircle.com/): Direct-Trade Kaffee, sogar mit Abo-Service.
- [filterfreunde.com](https://www.filterfreunde.com/shop/): Ein Start-Up Shop für Fair Trade Kaffee
- [flyingroasters.de](https://www.flyingroasters.de/shop/): Berliner Rösterei, auch mit Abo-Service.
- [elbgoldshop.com](https://www.elbgoldshop.com/): Hamburger Shop für von-Hand gerösteten Kaffee.
- [nurucoffee.com](https://www.nurucoffee.com/shop/): Äthiopischer Kaffee. Auch mit Abo-Service.
- [kaffeemacher.de](https://shop.kaffeemacher.ch/): Schweizer Kaffee und Zubehör wie Mühlen, Kannen & Maschinen.
- [quijote-kaffee.de](https://www.quijote-kaffee.de/shop/): Direktimport Kaffee
- [ttt-shop.de](https://www.ttt-shop.de/): Tees und Zubehör

#### Alkohol
- [bierothek.de](https://bierothek.de/): Biere größerer Marken aus aller Welt
- [brewcomer.com](https://www.brewcomer.com/shop/): Craftbeer und Microbrews aus aller Welt
- [conalco.de](https://www.conalco.de/): Spezialisiert auf Spirituosen, aber auch Weine.
- [craftbeerrockstars.de](https://craftbeerrockstars.de/shop/): Craftbeer aus aller Welt
- [craftbeer-shop.com](https://www.craftbeer-shop.com/): Craftbeer aus aller Welt
- [drank-dozijn.de](https://drankdozijn.de/): Oftmals gute Angebote für Single Malts
- [drinkology.de](https://www.drinkology.de/): Alles außer Bier & Wein
- [urban-drinks.de](https://www.urban-drinks.de/): Spirituosen aus aller Welt
- [vicampo.de](https://www.vicampo.de/): Weine. Gute Auswahl an Weinpaketen.
- [vinos.de](https://www.vinos.de/): Speziell für spanische Weine.
- [vivino.com](https://www.vivino.com/): Haben auch eine App zum Favoriten abspeichern...
- [weinquelle.com](https://www.weinquelle.com/): Shop aus Hamburg. Sehr tiefes Sortiment.

#### Nüsse
- [nusskauf.de](https://www.nusskauf.de/): Nüsse, Samen und Kerne, aber auch Trockenfrüchte und Müslis.
- [red-squirrels.com](https://www.red-squirrels.com/de/shop/): Kleiner Niederländischer Shop für rohe & geröstete Nüsse und Mischungen.

#### Süßigkeiten
- [kates-popcorn.de](https://kates-popcorn.de/): Popcorn in verschiedenen Geschmaksrichtungen
- [lambertz-shop.de](http://www.lambertz-shop.de/): Printen und Kekse aus Aachen.
- [printen.de](https://www.printen.de/): Aachener Printen der Bäckerei Klein
- [worldofsweets.de](https://www.worldofsweets.de/): Alle bekannten Süßwaren, aber auch aus aller Welt.

---------------------------------------------------------------------------------------------------------
### HAUS UND GARTEN
----
#### Wohnen
- [touva.com](https://www.trouva.com/): UK-based 'Lifestyle' Deko und Geschenke

#### Küche & Kochen
- [kochtail.de](https://www.kochtail.de/): Utensilien für Küche und Bar

#### Glas, Keramik & Porzellan
- [flaschenland.de](https://www.flaschenland.de/): Flaschen in allen Formen, Behälter aus Glas.
- [glaeserundflaschen.de](https://www.glaeserundflaschen.de/): Gläser, Flaschen, Verschlüsse, Etiketetten, etc.
- [keramik-geiger.de](https://www.keramik-geiger.de/shop/): Handgemachte Keramiken: Brottöpfe, Zwiebeltöpfe, etc.
- [mambocat.de](https://www.mambocat.de/): Einmach- und Aufbewahrungsgläser sowie "Kaffeezeit"-Porzellan

#### Pflanzen
- [dreschflegel-saatgut.de](https://www.dreschflegel-saatgut.de/): Biologisch angebautes Saatgut
- [brutblatt.net](https://brutblatt.net/shop/): Spezialshop für Brutblätter

---------------------------------------------------------------------------------------------------------
### SPIELE & SPIELZEUG
----
- [fantasywelt.de](https://www.fantasywelt.de/): Brettspiele, Trading Cards, Tabletops, etc.
- [miniaturicum](https://www.miniaturicum.de/): Tablestops im speziellen, aber auch Brett- & Kartenspiele. Fokus auf Fantasy.
- [spiele-offensive.de](https://www.spiele-offensive.de/): Brettspiele aller Art.
- [spieletaxi.de](https://spieletaxi.de/): Brettspiele & Kartenspiele
- [steinehelden.de](https://steinehelden.de/lego-online-shop/): Lego Sets und Einzelteile

---------------------------------------------------------------------------------------------------------
### ANDERE SACHEN
----
- [ben-anna.com](https://ben-anna.com/shop/): Naturkosmetik
- [eckladen1910.de](https://eckladen1910.de/): Alles Sachen die es 1910 auch schon gab, aber neuwertig.
- [kupfergrün.shop](https://kupfergruen.shop/): Vegane Kosmetik
- [wolkenseifen.de](https://www.wolkenseifen.de/): Seifen und andere Körperpflegeprodukte
- [bueromarkt-ag.de](https://www.bueromarkt-ag.de/): Schreibwaren und Bürobedarf
